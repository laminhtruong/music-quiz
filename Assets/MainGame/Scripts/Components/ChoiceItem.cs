﻿using System;
using UnityEngine;
using UnityEngine.UI;
using Doozy.Engine.UI;
using MusicQuiz;

public class ChoiceItem : MonoBehaviour
{
    [SerializeField] private Text TextTitle;
    [SerializeField] private Text TextArtist;
    [SerializeField] private GameObject ObjectCorrect;
    [SerializeField] private GameObject ObjectIncorrect;

    int mIndex = 0;
    int mCorrectIndex = 0;

    public static Action<int, bool> OnChoice;

    void Start()
    {
        GetComponent<UIButton>().OnClick.OnTrigger.Event.AddListener(OnClick);
    }

    public void SetInfo(Choice choice, int index, int correctIndex)
    {
        mIndex = index;
        mCorrectIndex = correctIndex;

        TextTitle.text = choice.title;
        TextArtist.text = choice.artist;

        ObjectCorrect.SetActive(false);
        ObjectIncorrect.SetActive(false);
    }

    public void SetInteractive(bool value)
    {
        GetComponent<UIButton>().Interactable = value;
    }

    void OnClick()
    {
        if (OnChoice != null)
        {
            bool correct = mIndex == mCorrectIndex;
            ObjectCorrect.SetActive(correct);
            ObjectIncorrect.SetActive(!correct);

            OnChoice(mIndex, correct);
        }
    }
}
