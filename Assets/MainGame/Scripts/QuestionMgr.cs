﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Newtonsoft.Json;
using MusicQuiz;

public class QuestionMgr : Singleton<QuestionMgr>
{
	List<Playlist> mPlaylists;
	List<Sprite> mPictures = new List<Sprite>();
	List<AudioClip> mSamples = new List<AudioClip>();

	void Start()
	{

	}

	// Update is called once per frame
	void Update()
	{

	}

	public void Load(string url, Action<bool> callback)
	{
		DownloadMgr.Instance.LoadText(url,
		progress =>
		{

		},
		json =>
		{
			if (json != null)
			{
				mPlaylists = JsonConvert.DeserializeObject<List<Playlist>>(json);
			}
			callback(json != null);
		});
	}

	public void LoadQuestions(string id, Action<float> progress, Action<List<Sprite>, List<AudioClip>, bool> callback)
	{
		Reset();
        LoadQuestions(id, 0, progress, callback);
	}

	public List<Playlist> GetPlaylists()
	{
		return mPlaylists;
	}

	List<Question> GetQuestionsById(string id)
	{
		return mPlaylists.FindAll(item => item.id == id)[0].questions;
	}

	void LoadQuestions(string id, int index, Action<float> progress, Action<List<Sprite>, List<AudioClip>, bool> callback)
	{
		List<Question> questions = GetQuestionsById(id);
		float percentPerItem = 1f / (questions.Count * 2);

		DownloadMgr.Instance.LoadTexture(questions[index].song.picture,
		percent =>
		{
			progress.Invoke(percent * percentPerItem + index * percentPerItem);
		},
		texture =>
		{
			if (texture != null)
			{
				Sprite sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(texture.width / 2, texture.height / 2));
				mPictures.Add(sprite);

				DownloadMgr.Instance.LoadAudioClip(questions[index].song.sample, AudioType.WAV,
				percent =>
				{
					progress.Invoke(percent * percentPerItem + (index + 1) * percentPerItem);
				},
				audioClip =>
				{
					if (audioClip != null)
					{
						mSamples.Add(audioClip);
                        if (index < questions.Count - 1)
                        {
                            LoadQuestions(id, index + 1, progress, callback);
                        }
                        else
                        {
                            callback(mPictures, mSamples,true);
                        }
					}
                    else
                    {
                        callback(mPictures, mSamples, false);
                    }
				});
			}
            else
            {
                callback(mPictures, mSamples, false);
            }
		});
	}

	void Reset()
	{
		mPictures.ForEach(picture => Destroy(picture));
		mPictures.Clear();

		mSamples.ForEach(sample => Destroy(sample));
		mSamples.Clear();
	}
}
