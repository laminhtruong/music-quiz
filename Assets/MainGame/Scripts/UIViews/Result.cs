﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using MusicQuiz;

public class Result : MonoBehaviour
{
    [SerializeField] private Text TextScore;
    [SerializeField] private GameObject PrefabResultItem;
	[SerializeField] private GameObject ResultContainer;

    List<GameObject> mItems = new List<GameObject>();

    bool mIsStarted = false;

    void OnEnable()
    {
        if (!mIsStarted)
        {
            mIsStarted = true;
            return;
        }
    }

    void Start()
    {

    }

    void Update()
    {

    }

    public void SetInfo(List<Question> questions, List<Sprite> pictures, List<int> userChoices)
    {
        int score = 0;
        for (int i = 0; i < questions.Count; i++)
        {
            GameObject item = null;
            if (i < mItems.Count)
            {
                item = mItems[i];
            }
            else
            {
                item = Instantiate(PrefabResultItem, ResultContainer.transform);
                mItems.Add(item);
            }

            ResultItem resultItem = item.GetComponent<ResultItem>();
            resultItem.SetInfo(questions[i], pictures[i], userChoices[i]);

            if (userChoices[i] == questions[i].answerIndex)
            {
                score++;
            }
        }

        TextScore.text = "" + score + " / " + questions.Count;
    }
}

