﻿using System;
using UnityEngine;
using UnityEngine.UI;
using Doozy.Engine.UI;
using System.Collections.Generic;
using MusicQuiz;

public class Ingame : MonoBehaviour
{
    [SerializeField] private Image ImagePicture;
    [SerializeField] private AudioSource AudioSample;
    [SerializeField] private Text TextTime;
    [SerializeField] private List<ChoiceItem> ChoiceItems;

    public static Action<List<Sprite>, List<int>> OnCompleted;

    enum STATE
    {
        INIT,
        ANSWER,
        RESULT,
        NEXT,
        FINISH
    }

    STATE mState;
    Timer mTimerTimeout = new Timer();
    List<Question> mQuestions;
    List<Sprite> mPictures;
    List<AudioClip> mSamples;
    List<int> mChoices = new List<int>();

    int mIndex;
    bool mIsStarted = false;

    void OnEnable()
    {
        if (!mIsStarted)
        {
            mIsStarted = true;
            return;
        }
    }

    void Start()
    {
        ChoiceItem.OnChoice += OnItemChoice;
    }

    void Update()
    {
        switch (mState)
        {
            case STATE.INIT:
                break;

            case STATE.ANSWER:
                mTimerTimeout.Update(Time.deltaTime);
                TextTime.text = mTimerTimeout.GetTime().ToString("#.##") + "s";
                if (mTimerTimeout.IsDone())
                {
                    mChoices.Add(-1);
                    TextTime.text = "0.00s";
                    SetState(STATE.RESULT);
                }
                break;

            case STATE.RESULT:
                mTimerTimeout.Update(Time.deltaTime);
                if (mTimerTimeout.IsDone())
                {
                    SetState(STATE.NEXT);
                }
                break;
        }
    }

    void SetState(STATE state)
    {
        mState = state;
        switch (mState)
        {
            case STATE.INIT:
                mIndex = 0;
                mChoices.Clear();
                SetState(STATE.ANSWER);
                break;

            case STATE.ANSWER:
                for (int i = 0; i < ChoiceItems.Count; i++)
                {
                    Question question = mQuestions[mIndex];
                    ChoiceItems[i].SetInfo(question.choices[i], i, question.answerIndex);
                    ChoiceItems[i].SetInteractive(true);
                }

                ImagePicture.sprite = mPictures[mIndex];
                AudioSample.clip = mSamples[mIndex];
                AudioSample.Play();

                mTimerTimeout.SetDuration(10);
                break;

            case STATE.RESULT:
                for (int i = 0; i < ChoiceItems.Count; i++)
                {
                    ChoiceItems[i].SetInteractive(false);
                }
                mTimerTimeout.SetDuration(1);
                break;

            case STATE.NEXT:
                if (mIndex < mQuestions.Count - 1)
                {
                    mIndex++;
                    SetState(STATE.ANSWER);
                }
                else
                {
                    SetState(STATE.FINISH);
                }
                break;

            case STATE.FINISH:
                if (OnCompleted != null)
                {
                    OnCompleted(mPictures, mChoices);
                }
                break;
        }
    }

    public void SetInfo(List<Question> questions, List<Sprite> pictures, List<AudioClip> samples)
    {
        mQuestions = questions;
        mPictures = pictures;
        mSamples = samples;

        SetState(STATE.INIT);
    }

    void OnItemChoice(int index, bool isCorrect)
    {
        mChoices.Add(index);
        SetState(STATE.RESULT);
    }
}

