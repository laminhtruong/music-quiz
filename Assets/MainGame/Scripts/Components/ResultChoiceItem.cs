﻿using System;
using UnityEngine;
using UnityEngine.UI;
using Doozy.Engine.UI;
using MusicQuiz;

public class ResultChoiceItem : MonoBehaviour
{
    [SerializeField] private Text TextTitle;
    [SerializeField] private Text TextArtist;
    [SerializeField] private GameObject ObjectCorrect;
    [SerializeField] private GameObject ObjectIncorrect;

    void Start()
    {

    }

    public void SetInfo(Choice choice, int index, int userChoice, int correctIndex)
    {
        TextTitle.text = choice.title;
        TextArtist.text = choice.artist;

        ObjectCorrect.SetActive(false);
        ObjectIncorrect.SetActive(false);

        if (index == userChoice)
        {
            ObjectCorrect.SetActive(userChoice == correctIndex);
            ObjectIncorrect.SetActive(userChoice != correctIndex);
        }

        GetComponent<Image>().color = index == correctIndex ? Color.green : Color.white;
    }
}
