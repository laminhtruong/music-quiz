﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using MusicQuiz;

public class Loading : MonoBehaviour
{
    [SerializeField] private Image ImageIcon;
    [SerializeField] private Text TextPercent;
    [SerializeField] private Text TextDescription;

    enum STATE
    {
        NONE,
    }

    STATE mState;
    bool mIsStarted = false;

    void OnEnable()
    {
        if (!mIsStarted)
        {
            mIsStarted = true;
            return;
        }
    }

    void Start()
    {

    }

    void Update()
    {
        ImageIcon.transform.eulerAngles -= new Vector3(0, 0, 60 * Time.deltaTime);
    }

    public void SetInfo(string description, float percent)
    {
        TextDescription.text = description;
        TextPercent.text = "" + Mathf.Floor(percent * 100) + "%";
    }
}

