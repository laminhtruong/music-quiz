﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MusicQuiz;

public class GameMgr : Singleton<GameMgr>
{
	[SerializeField] private MainMenu UIViewMainMenu;
	[SerializeField] private Loading UIViewLoading;
	[SerializeField] private Ingame UIViewIngame;
	[SerializeField] private Result UIViewResult;

	enum STATE
	{
		INIT,
		LOADING,
		INGAME,
		RESULT
	}

	STATE mState = STATE.INIT;
	Playlist mPlaylist;

	void Start()
	{
		MainMenu.OnPlaylistSelect += OnPlaylistSelect;
		Ingame.OnCompleted += OnGameCompleted;
	}

	void Update()
	{
		switch (mState)
		{
			case STATE.INIT:
				break;

			case STATE.LOADING:
				break;

			case STATE.INGAME:
				break;

			case STATE.RESULT:
				break;
		}
	}

	void SetState(STATE state)
	{
		mState = state;
		switch (mState)
		{
			case STATE.INIT:
				break;

			case STATE.LOADING:
                QuestionMgr.Instance.LoadQuestions(mPlaylist.id,
                progress =>
                {
					UIViewLoading.SetInfo("Loading questions ...", progress);
                },
                (pictures, samples, result) =>
                {
					UIViewLoading.SetInfo("Loading questions ...", 1);
					if (result)
					{
						UIViewIngame.SetInfo(mPlaylist.questions, pictures, samples);
						GameEventMgr.SendEvent("loading_completed");
					}
					else
					{
						// Control loading failed here
					}
                });
				break;

			case STATE.INGAME:
				break;

			case STATE.RESULT:
				break;
		}
	}

	void GameInit()
	{

	}

	void GameLoading()
	{
		GameEventMgr.SendEvent("playlist_selected");
		SetState(STATE.LOADING);
	}

	public void MainMenuInit()
	{
		UIViewMainMenu.Init();
	}

	void OnPlaylistSelect(Playlist playlist)
	{
		mPlaylist = playlist;
		GameLoading();
	}

	void OnGameCompleted(List<Sprite> pictures, List<int> choices)
	{
		UIViewResult.SetInfo(mPlaylist.questions, pictures, choices);
		GameEventMgr.SendEvent("game_completed");
	}
}
