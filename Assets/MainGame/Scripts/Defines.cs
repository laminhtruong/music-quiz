﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MusicQuiz
{
	public static class Config
	{
	}

    [System.Serializable]
    public class Playlist
    {
        public string id;
        public List<Question> questions;
        public string playlist;
    }

    [System.Serializable]
    public class Question
    {
        public string id;
        public int answerIndex;
        public List<Choice> choices;
        public Song song;

    }

    [System.Serializable]
    public class Choice
    {
        public string artist;
        public string title;
    }

    [System.Serializable]
    public class Song
    {
        public string id;
        public string title;
        public string artist;
        public string picture;
        public string sample;
    }
}

