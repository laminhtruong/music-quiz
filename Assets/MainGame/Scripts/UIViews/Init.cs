﻿using System.IO;
using UnityEngine;
using Doozy.Engine.Nody;
using System.Collections.Generic;
using MusicQuiz;

public class Init : MonoBehaviour
{
    enum STATE
    {
        NONE,
        INIT_DOOZY,
        LOAD_QUESTIONS,
        WAITING,
    }

    [SerializeField] private GraphController MainGraph;

    STATE mState;
    Timer mTimerWaiting = new Timer();

    void Awake()
    {
        mTimerWaiting.SetDuration(3);
    }

    void Start()
    {
        SetState(STATE.INIT_DOOZY);
    }

    void Update()
    {
        mTimerWaiting.Update(Time.deltaTime);
        switch (mState)
        {
            case STATE.INIT_DOOZY:
                if (MainGraph.Initialized)
                {
                    SetState(STATE.LOAD_QUESTIONS);
                }
                break;

            case STATE.LOAD_QUESTIONS:
                break;

            case STATE.WAITING:
                if (mTimerWaiting.IsDone())
                {
                    SetState(STATE.NONE);
                    GameEventMgr.SendEvent("init_completed");
                }
                break;
        }
    }

    void SetState(STATE state)
    {
        mState = state;
        switch (mState)
        {
            case STATE.INIT_DOOZY:
                break;

            case STATE.LOAD_QUESTIONS:
                string path = Path.Combine(Application.streamingAssetsPath, "coding-test-frontend-unity.json");
                QuestionMgr.Instance.Load(path, result =>
                {
                    if (result)
                    {
                        GameMgr.Instance.MainMenuInit();
                        SetState(STATE.WAITING);
                    }
                    else
                    {
                        // Control loading failed here if we load the file from internet
                    }
                });
                break;

            case STATE.WAITING:
                break;
        }
    }
}

