﻿using System;
using UnityEngine;
using UnityEngine.UI;
using Doozy.Engine.UI;
using MusicQuiz;

public class PlaylistItem : MonoBehaviour
{
    [SerializeField] private Image ImageCover;
    [SerializeField] private Text TextDescription;

    public static Action<Playlist> OnSelect;
    Playlist mPlaylist;

    void Start()
    {
        GetComponent<UIButton>().OnClick.OnTrigger.Event.AddListener(OnItemSelect);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SetInfo(Playlist playlist)
    {
        TextDescription.text = playlist.playlist;
        mPlaylist = playlist;
    }

    void OnItemSelect()
    {
        if (OnSelect != null)
        {
            OnSelect(mPlaylist);
        }
    }
}
