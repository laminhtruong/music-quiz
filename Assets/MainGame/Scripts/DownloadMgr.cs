﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine;

public class DownloadMgr : Singleton<DownloadMgr>
{
	void Start()
	{

	}

	void Update()
	{

	}

	string GetPath(string url)
	{
		bool isOnline = url.StartsWith("http");
		bool isAbsolute = url.StartsWith("file:///");

		if (isOnline)
		{
			string filename = Path.GetFileName(url);
			if (filename.Contains("?"))
			{
				filename = filename.Split('=')[1];
			}

			string filePath = Path.Combine(Application.persistentDataPath, filename);
			if (File.Exists(filePath))
			{
				Debug.Log("DownloaderMgr load file from cache " + filename);
				return "file:///" + filePath;
			}
			else
			{
				Debug.Log("DownloaderMgr load file from cloud " + url);
				return url;
			}
		}
		else if (isAbsolute)
		{
			return url;
		}
		else
		{
			Debug.Log("DownloaderMgr load file from streaming assets path " + url);
#if UNITY_ANDROID
            return Path.Combine(Application.streamingAssetsPath, url);
#else
			return "file:///" + Path.Combine(Application.streamingAssetsPath, url);
#endif
		}
	}

	public void LoadText(string url, Action<float> progress, Action<string> callback)
	{
		StartCoroutine(GetString(GetPath(url), progress, text =>
		{
			callback(text);
		}));
	}

	public void LoadTexture(string url, Action<float> progress, Action<Texture2D> callback)
	{
		StartCoroutine(GetTexture(GetPath(url), progress, texture =>
		{
			callback(texture);
		}));
	}

	public void LoadAudioClip(string url, AudioType type, Action<float> progress, Action<AudioClip> callback)
	{
		StartCoroutine(GetAudioClip(GetPath(url), type, progress, audioClip =>
		{
			callback(audioClip);
		}));
	}

	IEnumerator GetString(string url, Action<float> progress, Action<string> callback)
	{
		UnityWebRequest www = UnityWebRequest.Get(url);
		www.SendWebRequest();

		while (!www.isDone)
		{
			progress.Invoke(www.downloadProgress);
			yield return null;
		}

		if (www.isNetworkError || www.isHttpError)
		{
			callback(null);
		}
		else
		{
			callback(www.downloadHandler.text);
		}
	}

	IEnumerator GetTexture(string url, Action<float> progress, Action<Texture2D> callback)
	{
		UnityWebRequest www = UnityWebRequestTexture.GetTexture(url);
		www.SendWebRequest();

		while (!www.isDone)
		{
			progress.Invoke(www.downloadProgress);
			yield return null;
		}

		if (www.isNetworkError || www.isHttpError)
		{
			callback(null);
		}
		else
		{
			if (url.StartsWith("http"))
			{
				string id = Path.GetFileName(url).Split('=')[1];
				string savePath = Path.Combine(Application.persistentDataPath, id);
				File.WriteAllBytes(savePath, www.downloadHandler.data);
			}
			callback(DownloadHandlerTexture.GetContent(www));
		}
	}

	IEnumerator GetAudioClip(string url, AudioType type, Action<float> progress, Action<AudioClip> callback)
	{
		UnityWebRequest www = UnityWebRequestMultimedia.GetAudioClip(url, type);
		www.SendWebRequest();

		while (!www.isDone)
		{
			progress.Invoke(www.downloadProgress);
			yield return null;
		}

		if (www.isNetworkError || www.isHttpError)
		{
			callback(null);
		}
		else
		{
			if (url.StartsWith("http"))
			{
				string savePath = Path.Combine(Application.persistentDataPath, Path.GetFileName(url));
				File.WriteAllBytes(savePath, www.downloadHandler.data);
			}
			callback(DownloadHandlerAudioClip.GetContent(www));
		}
	}
}
