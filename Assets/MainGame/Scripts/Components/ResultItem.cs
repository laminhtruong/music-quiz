﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MusicQuiz;

public class ResultItem : MonoBehaviour
{
    [SerializeField] private Image ImagePicture;
    [SerializeField] private List<ResultChoiceItem> ChoiceItems;

    void Start()
    {

    }

    void Update()
    {

    }

    public void SetInfo(Question question, Sprite picture, int userChoice)
    {
        ImagePicture.sprite = picture;
        for (int i = 0; i < ChoiceItems.Count; i++)
        {
            ChoiceItems[i].SetInfo(question.choices[i], i, userChoice, question.answerIndex);
        }
    }
}
