﻿using System;
using UnityEngine;
using System.Collections.Generic;
using MusicQuiz;

public class MainMenu : MonoBehaviour
{
	[SerializeField] private GameObject PrefabPlaylistItem;
	[SerializeField] private GameObject PlaylistContainer;

	enum STATE
	{
		NONE,
	}

	public static Action<Playlist> OnPlaylistSelect;

	STATE mState;
	bool mIsStarted = false;

	void OnEnable()
	{
		if (!mIsStarted)
		{
			mIsStarted = true;
			return;
		}
	}

	void Start()
	{
		PlaylistItem.OnSelect = OnPlaylistSelect;
	}

	void Update()
	{
		switch (mState)
		{
			case STATE.NONE:
				break;
		}
	}

	void SetState(STATE state)
	{
		mState = state;
		switch (mState)
		{
			case STATE.NONE:
				break;
		}
	}

	public void Init()
	{
		List<Playlist> playlists = QuestionMgr.Instance.GetPlaylists();
		playlists.ForEach(item =>
		{
			GameObject objectPlaylistItem = Instantiate(PrefabPlaylistItem, PlaylistContainer.transform);
			PlaylistItem playlistItem = objectPlaylistItem.GetComponent<PlaylistItem>();

			playlistItem.SetInfo(item);
		});
	}
}

